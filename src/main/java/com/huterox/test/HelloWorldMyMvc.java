package com.huterox.test;

import com.huterox.spring.framework.annotation.HUController;
import com.huterox.spring.framework.annotation.HURequestMapping;
import com.huterox.spring.framework.annotation.HURequestParam;
import com.huterox.spring.framework.webmvc.servlet.HUModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@HUController
public class HelloWorldMyMvc {
    @HURequestMapping("/mymvc/hello")
    public HUModelAndView sayHello(HttpServletRequest req, HttpServletResponse resp,
                                   @HURequestParam("name") String name)
    {
        Map<String,Object> model = new HashMap<>();

        model.put("name",name);
        System.out.println(name);
        return new HUModelAndView("Hello", model);
    }
}
