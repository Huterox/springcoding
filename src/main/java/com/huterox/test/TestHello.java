package com.huterox.test;


import com.huterox.spring.framework.annotation.HUController;
import com.huterox.spring.framework.annotation.HURequestMapping;
import com.huterox.spring.framework.annotation.HURequestParam;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Deprecated
@HUController
public class TestHello {

    @HURequestMapping("/test/hello")
    public String sayHello(HttpServletRequest req, HttpServletResponse resp,
                           @HURequestParam("name") String name,
                           @HURequestParam("id") Integer id
                           ) throws IOException {

        String res = "Hello  -- "+name+"  --  "+id;
        PrintWriter writer = resp.getWriter();
        writer.write(res);
        writer.flush();
        writer.close();
        return res;
    }
}
