package com.huterox.spring.framework.webmvc.servlet;

import java.util.Map;

public class HUModelAndView {
    private String viewName;
    private Map<String,?> model;
    public HUModelAndView(String viewName) {
        this.viewName = viewName;
    }

    public HUModelAndView(String viewName, Map<String, ?> model) {
        this.viewName = viewName;
        this.model = model;
    }

    public String getViewName() {
        return viewName;
    }

    public Map<String, ?> getModel() {
        return model;
    }
}
