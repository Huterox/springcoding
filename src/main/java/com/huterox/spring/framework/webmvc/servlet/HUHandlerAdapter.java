package com.huterox.spring.framework.webmvc.servlet;

import com.huterox.spring.framework.annotation.HURequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;

public class HUHandlerAdapter {
    //执行分发过来的方法，然后把这个方法的返回值获取过来，如果它的返回值是Modelandview那么我们就去渲染这个东西
    //读取对应的HTML文件，然后把这个对应解析模板进行解析，这里是通过正则表达式去解析的

    public HUModelAndView handle(HttpServletRequest req, HttpServletResponse resp, HUHandlerMapping mappingHandler) throws InvocationTargetException, IllegalAccessException {
        //完成动态参数的匹配
        //获取请求参数
        Map<String,String[]> paramsMap = req.getParameterMap();

        Method method = mappingHandler.getMethod();

        Class<?>[] parameterTypes = method.getParameterTypes();
        Object[] paramValues = new Object[parameterTypes.length];
//        动态给值

        for (int i = 0; i < parameterTypes.length; i++) {
            Class<?> parameterType = parameterTypes[i];
            if(parameterType==HttpServletRequest.class){
                paramValues[i] = req;
                continue;
            }else if(parameterType==HttpServletResponse.class){
                paramValues[i] =resp;
                continue;
            }else{
                //目前只是针对四个基本类型的
                Annotation[][] pa = method.getParameterAnnotations();
                //pa[0]表示第一个参数 pa[0][0]第一个参数的第一个注解
                for(int j=0;j<pa.length;j++){
                    for(Annotation a:pa[i]){
                        if(a instanceof HURequestParam){
                            String paramName = ((HURequestParam) a).value();
                            if(!"".equals(paramName)){
                                String strings = Arrays.toString(paramsMap.get(paramName))
                                        .replaceAll("\\[|\\]","")
                                        .replaceAll("\\s","");//过滤
                                if(parameterType==String.class){

                                    paramValues[i] = strings;

                                }else if(parameterType==Integer.TYPE ||parameterType==Integer.class ){

                                    paramValues[i] = Integer.valueOf(strings);
                                }else if(parameterType==Double.TYPE ||parameterType==Double.class ){

                                    paramValues[i] = Double.valueOf(strings);
                                }else if(parameterType==Float.TYPE ||parameterType==Float.class ){
                                    paramValues[i] = Float.valueOf(strings);
                                }

                            }

                        }
                    }
                }

            }
        }

        Object result = method.invoke(mappingHandler.getController(),paramValues);
        if(result==null || result instanceof Void) return null;
        boolean isModeAndView = mappingHandler.getMethod().getReturnType() == HUModelAndView.class;
        if(isModeAndView){
            return (HUModelAndView) result;
        }
        return null;
    }
}
