package com.huterox.spring.framework.webmvc.servlet;

import java.io.File;
import java.util.Objects;

public class HUViewResolver {

    private final String DEFAULT_TEMPLATE_SUFFIX=".html";
    private File templateRootDir;
    public HUViewResolver(String templateRoot) {
        String templateRootPath = Objects.requireNonNull(this.getClass().getClassLoader().getResource(templateRoot)).getFile();
        this.templateRootDir = new File(templateRootPath);
    }

    public HUView resolverViewName(String viewName) {
        if(null==viewName || "".equals(viewName.trim())) return null;
        viewName = viewName.endsWith(DEFAULT_TEMPLATE_SUFFIX) ? viewName:(viewName+DEFAULT_TEMPLATE_SUFFIX);
        File templateFile = new File((this.templateRootDir.getPath()+"/"+viewName).replace("/+","/"));

        return new HUView(templateFile);
    }
}
