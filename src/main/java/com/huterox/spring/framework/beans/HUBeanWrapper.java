package com.huterox.spring.framework.beans;

public class HUBeanWrapper {
    private Object wrapperInstance;
    private Class<?> wrapperClass;
    public HUBeanWrapper(Object instance) {
        this.wrapperInstance = instance;
        this.wrapperClass = this.wrapperInstance.getClass();
    }

    public Object getWrapperInstance() {
        return wrapperInstance;
    }

    public Class<?> getWrapperClass() {
        return wrapperClass;
    }
}
